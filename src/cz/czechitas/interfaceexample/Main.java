package cz.czechitas.interfaceexample;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		MyMarkerInterface customClass = new CustomClass();
		
		List<String> myList = new ArrayList<>();
		List<String> anotherList = new LinkedList<>();
		
		printMyList(myList);
		printMyList(anotherList);

	}

	
	public static void printMyList(List someList) {
		someList.get(0);
	}
	
	public static void doSomething(CustomClass m) {
		m.printMe();
	}
}
