package cz.czechitas.interfaceexample;

public class CustomClass implements MyMarkerInterface {
	
	public void printMe() {
		System.out.println("I'm in the printMe() method!");
	}
}
