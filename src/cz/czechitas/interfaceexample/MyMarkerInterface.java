package cz.czechitas.interfaceexample;

public interface MyMarkerInterface {
	// Marker interface - doesn't have any methods
	public void printMe();
}
