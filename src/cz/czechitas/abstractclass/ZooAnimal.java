package cz.czechitas.abstractclass;

public abstract class ZooAnimal implements Animal {
	
	protected boolean alive = true;
	private int count = 5;
	
	public void breathe() {
		System.out.println("Inhaled and exhaled " + count + " times!");
	}
	
	public void die() {
		alive = false;
	}
	
	public abstract void eat();
	
	
}
