package cz.czechitas.abstractclass;

public interface Animal {
	public void breathe();
	
	public void die();
	
	default void getBorn() {
		System.out.println("Was born...");
	}
	
//	void getBorn();
}
