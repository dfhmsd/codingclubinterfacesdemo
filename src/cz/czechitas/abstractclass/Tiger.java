package cz.czechitas.abstractclass;

public class Tiger extends ZooAnimal {
	
	private int count = 10;

	@Override
	public void eat() {
		System.out.println("Had a great piece of meat!");
	}
	
	public void walk() {
		System.out.println("Pacing around the cage...");
	}
	
	@Override
	public void breathe() {
		super.breathe();
		System.out.println("Special tiger breath!");
	}

}
