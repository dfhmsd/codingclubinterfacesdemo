package cz.czechitas.abstractclass;

public class Main {

	public static void main(String[] args) {
		Tiger tiger1 = new Tiger();
		
		ZooAnimal tiger2 = new Tiger();
		
		tiger1.breathe();
		tiger2.breathe();
		tiger2.die();
		tiger1.eat();
		
		tiger1.walk();
//		tiger2.walk(); // not available in ZooAnimal

	}

}
