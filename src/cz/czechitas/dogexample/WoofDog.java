package cz.czechitas.dogexample;

public class WoofDog implements Dog {

	@Override
	public void bark() {
		System.out.println("Woof!");
	}
	
}
