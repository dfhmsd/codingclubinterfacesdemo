package cz.czechitas.dogexample;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Dog dog1 = new WoofDog();
		Dog dog2 = new AffDog();
		
		List<Dog> dogList = new ArrayList<>();
		dogList.add(dog1);
		dogList.add(dog2);
		
		dog1.bark();
		dog2.bark();

	}
	
	// implement an interface
	// inherit from the abstract class
	// inherit from a concrete class
	// standalone implementation

}
