package cz.czechitas.innerclass;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		Map<String, String> stringMap = new HashMap<>();
		
		stringMap.put("Czech Republic", "CZK");
		stringMap.put("Germany", "EUR");
		stringMap.put("Russia", "RUR");
		
		System.out.println("Germany is using " + stringMap.get("Germany"));
		
		Set<Map.Entry<String, String>> entries = stringMap.entrySet();
		
		for(Map.Entry<String, String> entry : entries) {
			System.out.println("...");
		}
	}

}
