Fork this project first to your Git account if you want to make any changes. Then clone it to your local repository.

In order to open this project in Eclipse, go to File -> Import -> Existing Project into the Workspace. Find the folder where your project is located and click Finish.

If you want to open it in ItelliJ IDEA, go to File -> Open. Find the folder where the project is located and click OK. It may prompt you to add project SDK (your Java version with which you can run the program).